﻿using Calculator.ALU.Contract;
using Calculator.Core.Contract;
using Calculator.UI.Display.Contract;
using Calculator.UI.Keyboard.Contract;
using Calculator.Memory.Contract;
using System;
using System.Collections.Generic;

namespace Calculator.UI.Main
{
    class Controler
    {
        private IDisplay display;
        private IKeyboard keyboard;
        private IMemoryOperations memoryOperations;
        private ICalculations calculations;
        private Operation currentOperation;

        private bool overwrite;
        private double number;

        private bool calculateMode;

        public Controler(IDisplay display, 
                         IKeyboard keyboard, 
                         IMemoryOperations memoryOperations,
                         ICalculations calculations)
        {
            this.display = display;
            this.keyboard = keyboard;
            this.memoryOperations = memoryOperations;
            this.calculations = calculations;

            this.keyboard.KeyPressed += OnKeyPressed;
            this.keyboard.EnableChanged += OnEnableChanged;
            Clear();
        }

        private void Clear()
        {
            memoryOperations.Reset();
            calculations.Reset();
            number = calculations.CurrentValue;
            UpdateDisplay();
            overwrite = true;
            calculateMode = false;
        }

        private void OnKeyPressed(object sender, KeyboardEventArgs e)
        {
            if (!ProcessOperation(e.Symbol))
                ProcessDigit(e.Symbol);
        }

        private void ProcessDigit(string symbol)
        {
            var cval = display.Value;
            var nval = cval.Equals("0") || overwrite ? "" : cval.ToString();
            if (!symbol.Equals(".") || !nval.Contains("."))
                nval += symbol;
            if (nval.StartsWith("."))
                nval = "0" + nval;
            display.Value = nval;
            number = ReadValue();
            overwrite = false;
            calculateMode = false;
        }

        private double ReadValue()
        {
            return Convert.ToDouble(display.Value);
        }

        private bool ProcessOperation(string symbol)
        {
            switch (symbol)
            {
                case "=":
                    Calculate();
                    calculateMode = true;
                    currentOperation = null;
                    return true;
                case "AC":
                    Clear();
                    return true;
                case "MC":
                    memoryOperations.MemoryClear();
                    return true;
                case "MR":
                    calculations.Reset();
                    number = memoryOperations.MemoryRetrive();
                    display.Value = number.ToString();
                    ProcessOperation("=");
                    return true;
                case "M+":
                    if (!calculateMode)
                        ProcessOperation("=");
                    memoryOperations.MemoryAdd(ReadValue());
                    return true;
                case "M-":
                    if (!calculateMode)
                        ProcessOperation("=");
                    memoryOperations.MemorySub(ReadValue());
                    return true;
                default:
                    return ProcessArithmeticOperation(symbol);
            }
        }

        private void Calculate()
        {
            calculations.PerformOperation(currentOperation, number);
            UpdateDisplay();
            overwrite = true;
        }

        private void UpdateDisplay()
        {
            display.Value = calculations.CurrentValue.ToString();
        }

        private bool ProcessArithmeticOperation(string symbol)
        {
            foreach (var operation in calculations.AllowedOperations)
                if (operation.Symbol.Equals(symbol))
                {
                    if (!calculateMode)
                        Calculate();
                    currentOperation = operation;
                    calculateMode = true;
                    return true;
                }
            return false;
        }

        private void OnEnableChanged(object sender, EnableEventArgs e)
        {
            display.IsEnabled = e.IsEnabled;
            if (e.IsEnabled)
                Clear();
        }
    }
}
