﻿using System.Runtime.Serialization;

namespace Calculator.ALU.Contract
{
    [DataContract]
    public class Operation
    {
        [DataMember]
        public string Name { get; private set; }

        [DataMember]
        public string Symbol { get; private set; }

        public Operation(string name, string symbol)
        {
            Name = name;
            Symbol = symbol;
        }

        public bool Equals(object other)
        {
            var o = other as Operation;
            if (o == null)
                return false;
            return o.Name == Name && o.Symbol == Symbol;
        }
    }
}
