﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Calculator.ALU.Contract
{
    [ServiceContract]
    public interface IALU
    {
        IEnumerable<Operation> AllowedOperations 
        {
            [OperationContract]
            get; 
        }

        [OperationContract]
        double PerformOperation(Operation operation, double number1, double number2);
    }
}
