﻿using TinyMVVMHelper;

namespace Calculator.UI.Keyboard
{
    class KeyboardViewModel : ViewModel
    {
        private Command clickCommand;
        private Command powerCommand;
        private bool enabled;

        public KeyboardViewModel()
        {
            enabled = false;
            clickCommand = new Command(p => { }, enabled);
            powerCommand = new Command(p => { IsEnabled = !IsEnabled; });
        }

        public Command ClickCommand { get { return clickCommand; } }

        public Command PowerCommand { get { return powerCommand; } }

        public bool IsEnabled 
        { 
            get { return enabled; } 
            set 
            { 
                enabled = value;
                FirePropertyChanged("IsEnabled");
                clickCommand.ExecutePredicate = x => enabled;
            } 
        }
    }
}
