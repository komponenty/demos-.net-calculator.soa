﻿using System.ServiceModel;

namespace Calculator.Memory.Contract
{
    [ServiceContract]
    public interface IRegisterOperator
    {
        [OperationContract]
        object Retrive(Register registerId);

        [OperationContract]
        void Store(Register regsiterId, object value);

        [OperationContract]
        void Clear(Register registerId);
    }
}
