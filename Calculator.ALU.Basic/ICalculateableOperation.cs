﻿using Calculator.ALU.Contract;

namespace Calculator.ALU
{
    public interface ICalculateableOperation
    {
        Operation Definiton { get; }

        double Calculate(double number1, double number2);
    }
}
