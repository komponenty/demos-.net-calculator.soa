﻿using Calculator.Core.Contract;
using Calculator.ALU.Contract;
using Calculator.Memory.Contract;
using System.Collections.Generic;
using System;

namespace Calculator.Core
{
    class Calculations : ICalculations
	{
        private IALU alu;
        private IRegisterOperator registerOperator;
        private Register accumulator;
        private Register lastOperation;

        public Calculations(IALU alu, IRegisterOperator registerOperator, Register accumulator, Register lastOperation)
        {
            this.alu = alu;
            this.registerOperator = registerOperator;
            this.accumulator = accumulator;
            this.lastOperation = lastOperation;
        }

		IEnumerable<Operation> ICalculations.AllowedOperations 
		{
            get { return alu.AllowedOperations; } 
		}

        void ICalculations.PerformOperation(Operation operation, double number)
		{
            if (operation == null)
                operation = FindOperator(registerOperator.Retrive(lastOperation) as string);
            else
                registerOperator.Store(lastOperation, operation.Name);
            double result = (operation == null) ? number : alu.PerformOperation(operation, Convert.ToDouble(registerOperator.Retrive(accumulator)), number);
            registerOperator.Store(accumulator, result);
		}

        private Operation FindOperator(string name)
        {
            foreach (var op in alu.AllowedOperations)
                if (op.Name.Equals(name))
                    return op;
            return null;
        }

        double ICalculations.CurrentValue
        {
            get { return Convert.ToDouble(registerOperator.Retrive(accumulator)); }
        }

        void ICalculations.Reset()
        {
            registerOperator.Clear(accumulator);
            registerOperator.Clear(lastOperation);
        }
	}
}
