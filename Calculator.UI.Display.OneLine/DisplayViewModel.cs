﻿using System.Windows.Media;
using TinyMVVMHelper;

namespace Calculator.UI.Display
{
    class DisplayViewModel : ViewModel
    {
        private string text = "";
        private bool enabled = false;
        private Brush backgroundColor;

        public string Text
        {
            get { return text; }
            set
            {
                if (enabled)
                {
                    text = value;
                    FirePropertyChanged("Text");
                }
            }
        }

        public bool IsEnabled
        {
            get { return enabled; }
            set
            {
                if (!value)
                    Text = "";
                enabled = value;
                FirePropertyChanged("IsEnabled");
                FirePropertyChanged("BackgroundColor");
            }
        }

        public Brush BackgroundColor
        {
            get { return enabled ? Brushes.WhiteSmoke : Brushes.LightSteelBlue; }
        }
    }
}
