﻿using Calculator.ALU.Contract;
using System.Collections.Generic;
using System.ServiceModel;

namespace Calculator.Core.Contract
{
    [ServiceContract]
    public interface ICalculations
    {
        IEnumerable<Operation> AllowedOperations 
        {
            [OperationContract]
            get; 
        }

        [OperationContract]
        void PerformOperation(Operation operation, double number);

        double CurrentValue 
        {
            [OperationContract]
            get; 
        }

        [OperationContract]
        void Reset();
    }
}
