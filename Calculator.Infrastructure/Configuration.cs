﻿using Calculator.Infrastructure.Modules;
using Ninject;

namespace Calculator.Infrastructure
{
    public class Configuration
    {
        private static IKernel container;

        public static IKernel Container
        {
            get
            {
                if (container == null)
                    container = ConfigureApp();
                return container;
            }
        }

        private static IKernel ConfigureApp()
        {
            var kernel = new StandardKernel(
                new DisplayModule(),
                new KeyboardModule(),
                new CoreModule()
            );

            return kernel;
        }
    }
}
