﻿using Calculator.Core.Contract;
using Calculator.Core;
using Ninject;
using Ninject.Modules;
using System.ServiceModel;

namespace Calculator.Infrastructure.Modules
{
    class CoreModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICalculations>().ToMethod(ctx =>
            {
                var factory = new ChannelFactory<ICalculations>("calc");
                return factory.CreateChannel();
            }).InSingletonScope();

            Bind<IMemoryOperations>().ToMethod(ctx =>
            {
                var factory = new ChannelFactory<IMemoryOperations>("mem");
                return factory.CreateChannel();
            }).InSingletonScope();
        }
    }
}
